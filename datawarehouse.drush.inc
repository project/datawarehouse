<?php

/**
 * @file
 * Migração de dados complementares ao Benfeitoria.
 */

/**
 * Implements hook_drush_command().
 */
function datawarehouse_drush_command() {
  $commands = array();
  $commands['data-warehouse-process'] = array(
    'aliases' => array(
      'dw-p',
    ),
    'description' => 'Process a PHP class that extends DataWarehouseBase class.',
    'arguments' => array(
      'class_name' => 'The name of the PHP class that extends DataWarehouseBase class.',
    ),
    'examples' => array(
      'drush dw-p YourDataWarehouse' => 'Process YourDataWarehouse class.',
    ),
  );
  return $commands;
}

function drush_datawarehouse_data_warehouse_process() {

  $args = func_get_args();

  if (empty($args)) {
    print t('You need to specify a DataWarehouseBase subclass.');
    die();
  }

  $DWClass = new $args[0]();

  if (isset($args[1]) && isset($args[2])) {
    $data = $DWClass->processData($args[1], $args[2]);
  }
  else {
    $data = $DWClass->processData();
  }

  $DWClass->saveSnapshot($data);
}