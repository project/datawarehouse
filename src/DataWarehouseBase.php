<?php

namespace Drupal\datawarehouse;

class DataWarehouseBase {

  /**
   * Defines the database table name and can be used in other places as a
   * machine name
   * @return string
   */
  public function getType() {
    throw new \Exception('You need to implement getType() method');
  }

  /**
   * The entity type from the entity used as reference for the DataWarehouse
   * @return string
   */
  public function getEntityType() {
    throw new \Exception('You need to implement getEntityType() method');
  }

  /**
   * @return array a Schema API structure defining the data that will be used
   * with the snapshots
   */
  public function getSchemaDefinitionFields() {
    throw new \Exception('You need to implement getSchemaDefinitionFields() method');
  }

  /**
   * Retrieve data needed to create the data warehouse and process it into
   * the expected format.
   * @return array
   */
  public function processData() {
    throw new \Exception('You need to implement processData() method');
  }

  /**
   * Retrieve data needed to create the data warehouse and process it into
   * the expected format.
   * @param $data array of processed data from the processData() method
   * @return boolean
   */
  public function saveSnapshot($data) {
    foreach ($data as $entity_id => $record) {
      foreach ($record as $key => $value) {
        if (is_array($record[$key])) {
          $record[$key] = json_encode($value);
        }
      }
      $record['entity_type'] = $this->getEntityType();
      $record['entity_id'] = $entity_id;
      $record['created'] = REQUEST_TIME;

      $result = drupal_write_record($this->getType(), $record);
      if ($result !== 1) {
        throw new \Exception('Error processing DataWarehouse for entity_id ' . $entity_id);
      }
    }
  }

  public function getHistoricalSeries($entity_type, $entity_id, $limit = FALSE) {
    $query = db_select($this::getType(), 'dw')
      ->fields('dw')
      ->condition('entity_type', $entity_type)
      ->condition('entity_id', $entity_id);
    if ($limit) {
      $query->range(0, $limit);
    }
    $result = $query->orderBy('created', 'desc')->execute();
    return $result->fetchAll();
  }

  protected function structureDataForComparison($total, $total_old) {
    $new = $total - $total_old;
    if ($total_old > 0) {
      $increase = ($total / $total_old) - 1;
    }
    else {
      $increase = 0;
    }
    return array(
      'total' => $total,
      'new' => $new,
      'increase' => $increase
    );
  }
}